*******************************************************
AEMFVP-A Architecture
*******************************************************

.. contents::

FVP Configuration
#################

When you launch the stack using the supplied scripts the FVP is configured to use 8 cores in two clusters. It will also enable a number of default options some of which support the use cases provided and some enable architectural features, the architecture version is supplied to the scripts as an argument and then is fed to the model.

The aemfvp-a stack is actually a combination of two stacks one to boot u-boot and one to boot uefi as follows:

::

    +-------+   +--------+   +--------+   +-------+
    |       |   |  UBOOT |   |        |   |       |
    |ARM-TF |-->|  (or)  |-->| Kernel |-->|Busybox|
    |       |   |  UEFI  |   |        |   |       |
    +-------+   +--------+   +--------+   +-------+

Once the kernel has booted it then launches a basic busybox filesystem. It is also possible to boot a distro straight from EDK2, this however requires the user to download a compatible image for the appropriate distro and specify the location of the downloaded image.

*Copyright (c) 2024, Arm Limited. All rights reserved.*
*Copyright (c) 2024, Linaro Limited. All rights reserved.*
