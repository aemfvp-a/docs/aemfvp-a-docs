*******************************************************
AEMFVP-A Test Cases
*******************************************************

.. contents::

Introduction
############

The AEMFVP-A stack supports a number of test cases, each test case is initialised using the boot.sh scripts located in model-scripts. A complete description of the usage of this script can be found at `Booting The System`_, however in it's basic form it can be executed for aemfvp-a as follows:

::

    ./boot.sh -p aemfvp-a -b <TEST_CASE>

The other available use cases are as follows:
   
U-Boot Boot
###########

In this case <TEST_CASE> is set to u-boot and this will boot the u-boot stack.

UEFI Boot
#########

In this case <TEST_CASE> is set to UEFI and this will boot the UEFI stack.

Distro Boot
###########

In this case a slightly different form the boot.sh command is required as follows.

::

    ./boot.sh -p aemfvp-a -b distro -d <DISTRO_IMAGE_PATH>

Where <DISTRO_IMAGE_PATH> is the absolute path to the distro image.

Currently the system is tested with the debian 12 cloud image, The cloud images for Debian can be obtained from the `Debian cloud images`_ page. A number of images from the previous releases, along with the daily builds of the latest release version are available on this page. The following cloud image has been used to validate the Debian distribution boot.

- Debian 12 Bookworm cloud image : https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-nocloud-arm64.raw

*Copyright (c) 2024, Arm Limited. All rights reserved.*
*Copyright (c) 2024, Linaro Limited. All rights reserved.*

.. _Booting The System: booting-the-system.rst
.. _Debian cloud images: https://cloud.debian.org/images/cloud/
