*******************************************************
Booting The System
*******************************************************

::     

    ./boot.sh \
             -p <PLATFORM> \
             -b <TEST_CASE> \
             -a <ARM_ARCH> \
             -d <DISTRO_PATH> \

The specified arguments are as follows:

::     

    -p <PLATFORM> The name of the platform you are running, so either aemfvp-a or aemfvp-a-rme
    -b <TEST> The test case you are running.
    -a <ARM_ARCH> The ARM architecture you are running, this is an optional argument that will currently default to v9-6 and the following architectures are supported:
        v8-9
        v9-6
    -d <DISTRO_PATH> The absolute path of the distro image you wish to boot, this argument is only required to be specified when -b is set to distro.


The system has two methods of operating the first will automatically start an xterm window for each UART and the second will simply start the FVP and rely on the user to connect to the FVP as required. If you do not want the system to start xterm by default. Then specify:

::

    export Automate=true

Prior to executing the boot.sh script, this is useful for starting the system on headless devices where there is no GUI. Once the system system is booted it will create a docker container and run the FVP version packaged for this release.  the docker container will be named in the form:

::

    <PLATFORM>-fvp-<TEST>-<GUID>

Where <PLATFORM> and <TEST> are copied form the command line and the <GUID> is a randomly generated ID for each container. The name is printed by the system at startup as follows:

::

    starting xterms for aemfvp-a-fvp-uefi-944b58dc-caca-4679-affb-bf4099554e05

The system will also on startup print the ports that are currently used by the telnet sessions connected to each of the FVP's uarts as follows:

::

    uart0:5000
    uart0:5001
    uart0:5002
    uart0:5003

If the system is running in automated mode then the user will need to manually connect to the telnet sessions of the FVP using the following:

::

    docker exec -it <CONTAINER_NAME> telnet 127.0.0.1 <PORT>

So for our example system to connect to UART0, the command would be:

::

    docker exec -it aemfvp-a-fvp-uefi-944b58dc-caca-4679-affb-bf4099554e05 telnet 127.0.0.1 5000

*Copyright (c) 2024, Arm Limited. All rights reserved.*
*Copyright (c) 2024, Linaro Limited. All rights reserved.*
