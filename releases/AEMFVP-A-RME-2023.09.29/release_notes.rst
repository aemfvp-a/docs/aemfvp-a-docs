AEMFVP-A-RME-2023.09.29
=======================

.. contents::

Release Description
-------------------

- This is the quarterly refresh of Arm's reference stack for CCA 
  software architecture.
- Arm's CCA has RMM, TF-A and Linux repos now EAC2 compliant for RME.


Test Coverage
-------------

The following tests have been completed using 11.23_9 version of the FVP on
both x86_64 and aarch64 host machines.

- buildroot boot Tests: boot to shell, validate 4 world boot
- Standard tests from TFTF
- KVM Unit Tests for Realm

Source Repositories
-------------------

The following source repositories have been integrated together in this release.
The associated tag or the hash in each of these repositories is listed as well.

* Linux

  - Source   : https://gitlab.arm.com/linux-arm/linux-cca
  - Tag      : cca-full/rmm-v1.0-eac2

* Kvmtool-CCA

  - Source   : https://gitlab.arm.com/linux-arm/kvmtool-cca
  - Tag      : cca/rmm-v1.0-eac2

* Trusted Firmware-A

  - Source   : https://git.trustedfirmware.org/TF-A/trusted-firmware-a.git/
  - Tag/Hash : 84de50c7d8ca416e504aedb228bb4cab6eed806f

* Hafnium

  - Source   : https://git.trustedfirmware.org/hafnium/hafnium.git
  - Tag/Hash : 5fe882d53d0a84d3c8abc5783b7308148de80856

* TF-RMM

  - Source   : https://git.trustedfirmware.org/TF-RMM/tf-rmm.git
  - Tag/Hash : 808551f8f4c3b719b01e227dfb3b4f2dc0e3d29f

* Buildroot

  - Source   : https://github.com/buildroot/buildroot.git
  - Tag/Hash : 2020.05

* KVM Unit Test

  - Source   : https://gitlab.arm.com/linux-arm/kvm-unit-tests-cca
  - Tag/Hash : cca/rmm-v1.0-eac2

* TF-A Test

  - Source   : https://git.trustedfirmware.org/TF-A/tf-a-tests.git
  - Tag/Hash : 6d8721db1753e0b9fc4252308186d5eb152252a8


Notes
-----

- Older release of Base aem CCA stack are at https://gitlab.arm.com/arm-reference-solutions/arm-reference-solutions-docs/-/tree/master/docs/aemfvp-a-rme/releases


--------------

*Copyright (c) 2023, Arm Limited. All rights reserved.*