AEMFVP-A-RME-2023.06.30
=======================

.. contents::

Release Description
-------------------

- This is the quarterly refresh of Arm's reference stack for CCA 
  software architecture.


Test Coverage
-------------

The following tests have been completed using 11.22_14 version of the FVP on
both x86_64 and aarch64 host machines.

- buildroot boot Tests: boot to shell, validate 4 world boot
- Standard tests from TFTF
- KVM Unit Tests for Realm

Source Repositories
-------------------

The following source repositories have been integrated together in this release.
The associated tag or the hash in each of these repositories is listed as well.

* Linux

  - Source   : https://gitlab.arm.com/linux-arm/linux-cca
  - Tag      : cca-full-rfc-v1

* Kvmtool-CCA

  - Source   : https://gitlab.arm.com/linux-arm/kvmtool-cca
  - Tag      : cca-rfc-v1

* Trusted Firmware-A

  - Source   : https://git.trustedfirmware.org/TF-A/trusted-firmware-a.git/
  - Tag/Hash : e87102f32bbdf0fa4b2892394cb4f2766321b9d4

* Hafnium

  - Source   : https://git.trustedfirmware.org/hafnium/hafnium.git
  - Tag/Hash : ef0627686b4965274eb646914f244497daa5b86c

* TF-RMM

  - Source   : https://git.trustedfirmware.org/TF-RMM/tf-rmm.git
  - Tag/Hash : 2e06f0dc7113c1e4e858f75c5070bdbf290fc63c

* Buildroot

  - Source   : https://github.com/buildroot/buildroot.git
  - Tag/Hash : 2020.05

* KVM Unit Test

  - Source   : https://gitlab.arm.com/linux-arm/kvm-unit-tests-cca
  - Tag/Hash : cca-rfc-v1

* TF-A Test

  - Source   : https://git.trustedfirmware.org/TF-A/tf-a-tests.git
  - Tag/Hash : 47b2cb2f314a4d1e0d43c90f65edb66a4b4f9475

--------------

*Copyright (c) 2023, Arm Limited. All rights reserved.*
