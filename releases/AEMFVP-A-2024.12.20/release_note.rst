AEMFVP-A-2024.12.20
=======================

.. contents::

Release Description
-------------------

- This is the quarterly refresh of Arm's combined aemfvp-a and aemfvp-a-rme stacks.

Release Details
---------------
This release of AEMFVP-A software stack comes with following main updates:

- Add additional PCI model parameters required for the TFTF tests.
- Add fix for missing edk2 subhook submodule.
- Change the FVP secure memory variable to 1 to enable TZC-400 security checking on aemfvp-a only.
- Add fix to resolve a number of FVP warnings that occur on boot.
- Upgrade the Debian image to the latest version.

Test Coverage
-------------

The following tests have been completed using 11.27.19 version of the FVP on
both x86_64 and aarch64 host machines for the aemfvp-a-rme stack.

- buildroot boot Tests: boot to shell, validate 4 world boot
- Standard tests from TFTF
- KVM Unit Tests for Realm
- Booting realm VM in realm security state.
- SSH connection Test to realm VM.
- Parallel boot of realm and normal world VM.
- UEFI boot of debian distro raw image on FVP.
- Debian distro raw image's UEFI boot in realm security state.
 
The following tests have been completed using 11.27.19 version of the FVP on
both x86_64 and aarch64 host machines for the aemfvp-a stack.

- Booting of u-boot into a shell.
- Booting of uefi into a shell.
- Booting of the debian distro into a shell.

Source Repositories
-------------------

aemfvp-a-rme
############

The following source repositories have been integrated together in this release.
The associated tag or the hash in each of these repositories is listed as well.

* ACPICA

  - Source   : https://github.com/acpica/acpica.git
  - Tag/Hash : R06_28_23

* Buildroot

  - Source   : https://github.com/buildroot/buildroot.git
  - Tag/Hash : 2024.08

* EDK2-platforms

  - Source   : https://gitlab.arm.com/linux-arm/edk2-platforms-cca.git
  - Tag/Hash : d5101eced3216902c126caf9e0cf139c9d9102fe

* EDK2

  - Source   : https://gitlab.arm.com/linux-arm/edk2-cca.git
  - Tag/Hash : 535c33f1f220417edddf2eeb4e714e2ed64062bd

* Hafnium

  - Source   : https://git.trustedfirmware.org/hafnium/hafnium.git
  - Tag/Hash : v2.12-rc0

* Kvmtool-CCA

  - Source   : https://gitlab.arm.com/linux-arm/kvmtool-cca.git
  - Tag/Hash : 54241e378586f01460afdf295e9bf41f3b52f4bd

* KVM Unit Test

  - Source   : https://gitlab.arm.com/linux-arm/kvm-unit-tests-cca.git
  - Tag/Hash : 77ee4332da6c9d3c6c947079bc4d755ba87da32c

* Linux

  - Source   : https://gitlab.arm.com/linux-arm/linux-cca.git
  - Tag/Hash : abe5cd5b20e2895ee880fdce0a113eedcf128a80

* TF-RMM

  - Source   : https://git.trustedfirmware.org/TF-RMM/tf-rmm.git
  - Tag/Hash : refs/tags/tf-rmm-v0.6.0

* Trusted Firmware-A

  - Source   : https://git.trustedfirmware.org/TF-A/trusted-firmware-a.git/
  - Tag/Hash : refs/tags/v2.12.0

* TF-A Test

  - Source   : https://git.trustedfirmware.org/TF-A/tf-a-tests.git
  - Tag/Hash : refs/tags/v2.12.0

aemfvp-a
########

* ACPICA

  - Source   : https://github.com/acpica/acpica.git
  - Tag/Hash : R06_28_23

* Busybox

  - Source   : https://github.com/mirror/busybox
  - Tag/Hash : 1_36_0

* Grub

  - Source   : https://git.savannah.gnu.org/git/grub
  - Tag/Hash : grub-2.06

* Linux

  - Source   : https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux
  - Tag/Hash : v6.12

* mbedtls

  - Source   : https://github.com/ARMmbed/mbedtls
  - Tag/Hash : v3.6.0

* tf-a

  - Source   : https://git.trustedfirmware.org/TF-A/trusted-firmware-a
  - Tag/Hash : v2.12.0

* openssl

  - Source   : https://github.com/openssl/openssl
  - Tag/Hash : openssl-3.1.1

* u-boot

  - Source   : https://git.denx.de/u-boot
  - Tag/Hash : v2024.10

* EDK2

  - Source   : https://github.com/tianocore/edk2.git
  - Tag/Hash : edk2-stable202411

* EDK2-platforms

  - Source   : https://github.com/tianocore/edk2-platforms.git
  - Tag/Hash : b1be341ee6f9ffde833779cf129660e510b8eb61

--------------

*Copyright (c) 2024, Arm Limited. All rights reserved.*
*Copyright (c) 2024, Linaro Limited. All rights reserved.*
