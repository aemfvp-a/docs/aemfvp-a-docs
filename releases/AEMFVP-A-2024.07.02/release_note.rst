AEMFVP-A-2024.07.02
=======================

.. contents::

Release Description
-------------------

- This is the quarterly refresh of Arm's combined aemfvp-a and aemfvp-a-rme stacks.

Release Details
---------------
This release of AEMFVP-A software stack comes with following main updates:

- Merge the aemfvp-a and aemfvp-a-rme product scripts.
- Add docker based container management for FVP's.
- Simplification of running of test cases using model scripts.
- Incremental updates to the stack components.

Test Coverage
-------------

The following tests have been completed using 11.25.15 version of the FVP on
both x86_64 and aarch64 host machines for the aemfvp-a-rme stack.

- buildroot boot Tests: boot to shell, validate 4 world boot
- Standard tests from TFTF
- KVM Unit Tests for Realm
- Booting realm VM in realm security state.
- SSH connection Test to realm VM.
- Parallel boot of realm and normal world VM.
- UEFI boot of debian distro raw image on FVP.
- Debian distro raw image's UEFI boot in realm security state.
 
The following tests have been completed using 11.25.15 version of the FVP on
both x86_64 and aarch64 host machines for the aemfvp-a stack.

- Booting of u-boot into a shell.
- Booting of uefi into a shell.
- Booting of the debian distro into a shell.

Source Repositories
-------------------

aemfvp-a-rme
############

The following source repositories have been integrated together in this release.
The associated tag or the hash in each of these repositories is listed as well.

* ACPICA

  - Source   : https://github.com/acpica/acpica.git
  - Tag/Hash : ada5b805eaa7480930082af9bc3d689c6f181329

* Buildroot

  - Source   : https://github.com/buildroot/buildroot.git
  - Tag/Hash : 2024.02.1

* EDK2-platforms

  - Source   : https://github.com/tianocore/edk2-platforms.git
  - Tag/Hash : adea4ad93c3664d879f951aea97a956c52e42ec1

* EDK2

  - Source   : https://github.com/tianocore/edk2.git
  - Tag/Hash : edk2-stable202405

* Hafnium

  - Source   : https://git.trustedfirmware.org/hafnium/hafnium.git
  - Tag/Hash : 41e8d5b1f805e882554b567e587c0eed5a81c49d

* Kvmtool-CCA

  - Source   : https://gitlab.arm.com/linux-arm/kvmtool-cca.git
  - Tag/Hash : e54eaed9536a2bb1bbf06d5f7210a8ba9707c728

* KVM Unit Test

  - Source   : https://gitlab.arm.com/linux-arm/kvm-unit-tests-cca.git
  - Tag/Hash : 1630c19b92e2a2fd3322598d96ff75b6e4eb32e5

* Linux

  - Source   : https://gitlab.arm.com/linux-arm/linux-cca.git
  - Tag/Hash : 19ff5f60db625384d1a66158952e42027f9e1d8e

* TF-RMM

  - Source   : https://git.trustedfirmware.org/TF-RMM/tf-rmm.git
  - Tag/Hash : 31a9f62fccd00ec018e682feaa9d47a7d3f5aa7e

* Trusted Firmware-A

  - Source   : https://git.trustedfirmware.org/TF-A/trusted-firmware-a.git/
  - Tag/Hash : a6cb061b62eda75db46bacb480f5027b6e38de5b

* TF-A Test

  - Source   : https://git.trustedfirmware.org/TF-A/tf-a-tests.git
  - Tag/Hash : eaf60b4cad7d34afedb3259a4dd6164b39cb9c0e

aemfvp-a
########

* ACPICA

  - Source   : https://github.com/acpica/acpica.git
  - Tag/Hash : R06_28_23

* Busybox

  - Source   : https://github.com/mirror/busybox
  - Tag/Hash : 1_35_0

* Grub

  - Source   : https://git.savannah.gnu.org/git/grub
  - Tag/Hash : grub-2.06

* Linux

  - Source   : https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux
  - Tag/Hash : v6.9.5

* mbedtls

  - Source   : https://github.com/ARMmbed/mbedtls
  - Tag/Hash : v3.6.0

* tf-a

  - Source   : https://git.trustedfirmware.org/TF-A/trusted-firmware-a
  - Tag/Hash : v2.11.0

* openssl

  - Source   : https://github.com/openssl/openssl
  - Tag/Hash : openssl-3.1.1

* u-boot

  - Source   : https://git.denx.de/u-boot
  - Tag/Hash : v2024.04

* EDK2

  - Source   : https://github.com/tianocore/edk2.git
  - Tag/Hash : edk2-stable202405

* EDK2-platforms

  - Source   : https://github.com/tianocore/edk2-platforms.git
  - Tag/Hash : adea4ad93c3664d879f951aea97a956c52e42ec1

--------------

*Copyright (c) 2024, Arm Limited. All rights reserved.*
*Copyright (c) 2024, Linaro Limited. All rights reserved.*
