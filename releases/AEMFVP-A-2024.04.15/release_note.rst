AEMFVP-A-2024.04.15
=======================

.. contents::

Release Description
-------------------

- This is the quarterly refresh of Arm's reference stack for CCA software 
  architecture.
- Base AEM stack now supports UEFI boot of distro debian image on FVP and 
  as realm machine in rme security state.
- Arm's CCA has RMM, TF-A and Linux repos now EAC5 compliant for RME.

Release Details
---------------
This release of AEM-A-RME FVP software stack comes with following main updates
and system's version details:

- UEFI boot of distro image supported at FVP and on rme security state machines.
- CCA spec has been updated to EAC5 release for TF-RMM with supporting linux 
  and kvm repos.
- Build supported on Ubuntu Jammy *22.04*.
- GCC version for build updated to *12.3.rel1*.
- Buildroot version update to *2023.08*.
- Linux kernel *v6.7.0*.
- Scalable Vector extension enabled in FVP runs.
- ARM public FVP version used *11.25_15*.
- Docker version *26.0.0*.

Test Coverage
-------------

The following tests have been completed using 11.25.15 version of the FVP on
both x86_64 and aarch64 host machines.

- buildroot boot Tests: boot to shell, validate 4 world boot
- Standard tests from TFTF
- KVM Unit Tests for Realm
- Booting realm VM in realm security state.
- SSH connection Test to realm VM.
- Parallel boot of realm and normal world VM.
- UEFI boot of debian distro raw image on FVP.
- Debian distro raw image's UEFI boot in realm security state.

Source Repositories
-------------------

The following source repositories have been integrated together in this release.
The associated tag or the hash in each of these repositories is listed as well.

* Linux

  - Source   : https://gitlab.arm.com/linux-arm/linux-cca.git
  - Branch   : cca-full/rmm-v1.0-eac5

* Kvmtool-CCA

  - Source   : https://gitlab.arm.com/linux-arm/kvmtool-cca.git
  - Branch   : cca/rmm-v1.0-eac5

* Trusted Firmware-A

  - Source   : https://git.trustedfirmware.org/TF-A/trusted-firmware-a.git/
  - Tag/Hash : a6cb061b62eda75db46bacb480f5027b6e38de5b

* Hafnium

  - Source   : https://git.trustedfirmware.org/hafnium/hafnium.git
  - Tag/Hash : 41e8d5b1f805e882554b567e587c0eed5a81c49d

* TF-RMM

  - Source   : https://git.trustedfirmware.org/TF-RMM/tf-rmm.git
  - Tag/Hash : b631c1295230548dd02ea4e43905ec29a1f32b9f

* Buildroot

  - Source   : https://github.com/buildroot/buildroot.git
  - Branch   : 2023.08

* KVM Unit Test

  - Source   : https://gitlab.arm.com/linux-arm/kvm-unit-tests-cca.git
  - Branch   : cca/rmm-v1.0-eac5

* TF-A Test

  - Source   : https://git.trustedfirmware.org/TF-A/tf-a-tests.git
  - Tag/Hash : eaf60b4cad7d34afedb3259a4dd6164b39cb9c0e

* EDK2

  - Source   : https://gitlab.arm.com/linux-arm/edk2-cca.git
  - Branch   : 2802_arm_cca_rmm-v1.0-eac5

* EDK2-platforms

  - Source   : https://gitlab.arm.com/linux-arm/linux-arm/edk2-platforms-cca.git
  - Branch   : 2802_arm_cca_rmm-v1.0-eac5

* ACPICA

  - Source   : https://github.com/acpica/acpica.git
  - Tag/Hash : ada5b805eaa7480930082af9bc3d689c6f181329

--------------

*Copyright (c) 2024, Arm Limited. All rights reserved.*
