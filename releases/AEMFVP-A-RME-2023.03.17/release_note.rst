AEMFVP-A-RME-2023.03.17
=======================

.. contents::

Release Description
-------------------

- This is initial release of reference integration stack for Arm's reference
  CCA software architecture. It demonstrates support for Arm's Realm
  Management Extension (RME) architecture feature implemented in a series of
  open source components.


Test Coverage
-------------

The following tests have been completed using 11.20.15 version of the FVP on
both x86_64 and aarch64 host machines.

- buildroot boot Tests: boot to shell, validate 4 world boot
- Standard tests from TFTF
- KVM Unit Tests for Realm

Source Repositories
-------------------

The following source repositories have been integrated together in this release.
The associated tag or the hash in each of these repositories is listed as well.

* Linux

  - Source   : https://gitlab.arm.com/linux-arm/linux-cca
  - Tag      : cca-full-rfc-v1

* Kvmtool-CCA

  - Source   : https://gitlab.arm.com/linux-arm/kvmtool-cca
  - Tag      : cca-rfc-v1

* Trusted Firmware-A

  - Source   : https://git.trustedfirmware.org/TF-A/trusted-firmware-a.git/
  - Tag/Hash : 3d2da6f5d3a931d97e0294f0a565b1810a55ab98

* Hafnium

  - Source   : https://git.trustedfirmware.org/hafnium/hafnium.git
  - Tag/Hash : 4df5520d166a2955566ce8826a5254f7f7ff5fdc

* TF-RMM

  - Source   : https://git.trustedfirmware.org/TF-RMM/tf-rmm.git
  - Tag/Hash : 61bdf4e8418f8c34def91a0ea6e4057f535e211a

* Buildroot

  - Source   : https://github.com/buildroot/buildroot.git
  - Tag/Hash : 2020.05

* KVM Unit Test

  - Source   : https://gitlab.arm.com/linux-arm/kvm-unit-tests-cca
  - Tag/Hash : cca-rfc-v1

* TF-A Test

  - Source   : https://git.trustedfirmware.org/TF-A/tf-a-tests.git
  - Tag/Hash : 8d80d6501af8a903b9a828ee57a6f490fc89bc18

--------------

*Copyright (c) 2023, Arm Limited. All rights reserved.*
