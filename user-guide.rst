*******************************************************
FVP_Base_RevC-2xAEMvA Reference Stacks
*******************************************************

.. contents::

Introduction
############

The Armv-A Base RevC AEM FVP (Fixed Virtual Platform) is an evolution of the base platform, enhanced to support the exploration of system level virtualization.

It is an Architecture Envelope Model (AEM) which incorporates two AEM clusters, each of which can be configured with up to four cores.

Currently two stacks are provided that run on this platform and are described in the subsequent sections of this guide.

AEMFVP-A Stack
##############

The AEMFVP-A stack is intended to demonstrate a u-boot and a uefi based system running on the FVP. Further Information information on the stack architecture can be found at `AEMFVP-A Architecture`_

AEMFVP-A-RME Stack
##################

The AEMFVP-A-RME stack is intended to demonstrate ARM CCA. The stack provides a number of use cases for exercising the features of CCA. Further Information information on the stack architecture can be found at `AEMFVP-A-RME Architecture`_

Validated build environments
############################

The build-system has been validated in the included Docker container and directly in Ubuntu 24.04 LTS but **only the Docker container is fully supported**.

Pre-requisites
##############

- x86_64 or aarch64 host environment
- Minimum free disk space of 50 GB;
- Docker installed
- Git installed
- Commands provided in this guide are executed from a ``bash`` shell
  environment.

To install Docker on the system, follow the `Standard Installation of Docker`_ page.

Git Setup
#########

Configure git user name and email address as follows:

::

    git config --global user.name "<your-name>"
    git config --global user.email "<your-email@example.com>"

Fetching the Stack
###################

The reference stacks utilise the repo command and associated manifests to fetch all of the components required to build the stack. Therefore to fetch the repo tool, create and initialise the workspace the user must execute the following commands:

::

    curl https://storage.googleapis.com/git-repo-downloads/repo > /usr/bin/repo
    chmod a+x /usr/bin/repo ;
    mkdir <AEMFVP_A_WORKSPACE>
    cd <AEMFVP_A_WORKSPACE>
    repo init \
          -u https://git.gitlab.arm.com/aemfvp-a/aemfvp-a-manifests.git \
          -m pinned-<PLATFORM>.xml \
          -b refs/tags/AEMFVP-A-2024.12.20 \
          --depth=1
    repo sync -c -j $(nproc) --force-sync --no-clone-bundle

Where <AEMFVP_A_WORKSPACE> is the name of the folder where you wish to store your workspace and <PLATFORM> is the stack name so either aemfvp-a or aemfvp-a-rme depending on which stack you wish to build.

Building the Docker Build and Emulation Environment
###################################################

In order to build and run the stacks two docker containers are provided. The first is used to build the stack and the second is used to run up the built stack using the FVP. Docker can be installed as follows:

::

    cd container-scripts/aemfvp-a
    ./container.sh build

Build the Stack
###############

The stack can be built by running up the build container and building the stack by running the following commands:

::

    ./container.sh -v <FULL_PATH_TO_WORKSPACE> run
    cd <FULL_PATH_TO_WORKSPACE>
    ./build-scripts/aemfvp-a/build-stack.sh -p <PLATFORM> all

    where:
        <FULL_PATH_TO_WORKSPACE> is the full absolute path to the workspace you created earlier
        <PLATFORM> is the name of the stack you wish to build so either aemfvp-a or aemfvp-a-rme

Run up the Test Cases
#####################

Once the stack build has completed the model can be booted directly from the container, however you may need to run the following command to allow it to access the socket:

::

    sudo chmod 777 /var/run/docker.sock

Then you can run the following command to boot the test cases:

::

    ./model-scripts/aemfvp-a/boot.sh -p <PLATFORM> -b <TEST_CASE>
    where:
        <PLATFORM> is the name of the stack so either aemfvp-a or aemfvp-a-rme
        <TEST_CASE> is the name of the test case.

This will create another container within which and FVP is run with the appropriate artifacts.  
A detailed description of the test cases and model usage is provided in following for each stack

- `Test Cases AEMFVP-A`_
- `Test Cases AEMFVP-A-RME`_

However a basic shell can be run up using the following command for aemfvp-a:

::

    ./model-scripts/aemfvp-a/boot.sh -p aemfvp-a -b uefi

And the following for aemfvp-a-rme:

::

    ./model-scripts/aemfvp-a/boot.sh -p aemfvp-a-rme -b shell


Report security vulnerabilities
###############################

For reporting security vulnerabilities, please refer `Vulnerability reporting`_
page.

*Copyright (c) 2024, Arm Limited. All rights reserved.*
*Copyright (c) 2024, Linaro Limited. All rights reserved.*

.. _AEMFVP-A Architecture: aemfvp-a/architecture.rst
.. _AEMFVP-A-RME Architecture: aemfvp-a-rme/architecture.rst
.. _Test Cases AEMFVP-A: aemfvp-a/test-cases.rst
.. _Test Cases AEMFVP-A-RME: aemfvp-a-rme/test-cases.rst
.. _Vulnerability reporting: vulnerability-reporting.rst
.. _Standard Installation of Docker: https://docs.docker.com/engine/install/
