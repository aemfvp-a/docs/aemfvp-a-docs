*******************************************************
AEMFVP-A-RME Architecture
*******************************************************

.. contents::

Introduction
############

This is a reference integration stack for Arm's Confidential Compute Architecture (CCA) software's implementation. It demonstrates support for Arm's Realm Management Extension (RME) architecture feature implemented in a  series of open source components.

Below is an overview of the software stack and its components:

.. image:: aemfvp-a-rme/images/sw-components.png
  :width: 1130
  :height: 480
  :alt: SW components diagram


The software stack for this platform includes an integration of `Linux-CCA`_, `Kvmtool-CCA`_, `Trusted-Firmware-A`_,  `Hafnium`_, `TF-RMM`_ and other
components.

In this document, instructions to build and run a firmware and Linux stack on Arm's Base AEM (Architecture Envelope Model) FVP platform model is provided.

Arm Confidential Compute
########################

Confidential compute is a broad term for technologies that reduce the need to trust a computing infrastructure, such as the need for processes to trust operating system (OS) kernel and the need for virtual machines to trust hypervisors.

Confidential compute performs computation within a trustworthy hardware-backed secure environment. This shields code and data from observation or modification by privileged software and hardware agents. Any application or operating system executing in a confidential computing environment can expect to execute in isolation from any non-trusted agent in the rest of the system.

More details on Arm CCA can be found at our official documentation here at 
`Arm Confidential Compute Architecture`_

The following diagram shows the software components in an Arm CCA system.

.. image:: aemfvp-a-rme/images/CCA-Software-components.png
  :width: 867
  :height: 416
  :alt: CCA Software components

Arm Realm Management Extension (RME)
####################################

RME supports a new type of attestable isolation environment called a Realm. Realms allow lower-privileged software, such as application or a Virtual Machine to protect its content and execution from attacks by higher-privileged software, such as an OS or a hypervisor. Higher-privileged software retains the responsibility for allocating and managing the resources that are utilized by a Realm, but cannot access its contents, nor affect its execution flow. The hypervisor however, manages system resources.

The diagram below gives the security model for Arm CCA. For more details refer
to the `Arm CCA security model`_.

.. image:: aemfvp-a-rme/images/RME-security-model.png
  :width: 660
  :height: 416
  :alt: RME security model

RME architecture
################

Realm Management Extension or RME is an extension to Arm v9 architecture that defines the set of hardware features and properties that are required to comply with the Arm CCA architecture. As shown in above diagram the RME extends the  Arm security model to four states, those are -

- Secure state
- Non-secure state
- Realm state
- Root state

SCR_EL3.[NSE,NS] controls PE security state.

+------------------+----------------+
| SCR_EL3.[NSE,NS] | Security state |
+==================+================+
|     0b00         |   Secure       |
+------------------+----------------+
|     0b01         |   Non-secure   |
+------------------+----------------+
|     0b10         |   NA           |
+------------------+----------------+
|     0b11         |   Realm        |
+------------------+----------------+

There is no encoding for root state. While in Exception level 3, the current security state is always root, regardless of the value of SCR_EL3.{NSE,NS}.

RME provides hardware-based isolation. In that,

- Execution in the Realm security state cannot be observed or modified by an agent associated with either the Non-secure or the Secure security state.
- Execution in the Secure security state cannot be observed or modified by an agent associated with either the Non-secure or the Realm security state.
- Execution in the Root security state cannot be observed or modified by an agent associated with any other security state.
- Memory assigned to the Realm security state cannot be read or modified by an agent associated with either the Non-secure or the Secure security state.
- Memory assigned to the Secure security state cannot be read or modified by an agent associated with either the Non-secure or the Realm security state.
- Memory assigned to the Root security state cannot be read or modified by an agent associated with any other security state.

Realm World
###########

The Realm world provides an execution environment for VMs that is isolated from the Normal and Secure worlds. VMs require control from the Host in the Normal world. To allow for full control of Realm creation and execution, the Arm CCA system provides:

- Realm Management Extension, which are the hardware extensions that are required by the architecture to allow isolated Realm VM execution.
- Realm Management Monitor, which is part of the firmware that is required to manage Realm creation and execution under requests from a Normal world Host.

The Realm Management Monitor (RMM) executes at EL2 in Realm security state (R-EL2). Its responsibilities are to:

- Provide an execution environment for Realms, which run at R-EL1 / R-EL0.
- Isolate Realms from one another.

*Copyright (c) 2024, Arm Limited. All rights reserved.*
*Copyright (c) 2024, Linaro Limited. All rights reserved.*

.. _Linux-CCA: https://gitlab.arm.com/linux-arm/linux-cca
.. _Kvmtool-CCA: https://gitlab.arm.com/linux-arm/kvmtool-cca
.. _Trusted-Firmware-A: https://trustedfirmware-a.readthedocs.io/en/latest/
.. _Hafnium: https://review.trustedfirmware.org/plugins/gitiles/hafnium/hafnium/+/HEAD/README.md
.. _TF-RMM: https://tf-rmm.readthedocs.io/en/latest/
.. _Arm Confidential Compute Architecture: https://www.arm.com/architecture/security-features/arm-confidential-compute-architecture
.. _Arm CCA security model: https://developer.arm.com/documentation/DEN0096/latest/
