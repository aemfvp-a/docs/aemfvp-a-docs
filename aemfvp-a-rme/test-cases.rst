*******************************************************
AEMFVP-A-RME Test Cases
*******************************************************

.. contents::

Introduction
############

The AEMFVP-A-RME stack supports a number of test cases as follows, each test case is initialised using the boot.sh scripts located in model-scripts. A complete description of the usage of this script can be found at `Booting The System`_, however in it's basic form it can be executed for aemfvp-a-rme as follows:

::

    ./model-scripts/aemfvp-a/boot.sh -p aemfvp-a-rme -b <TEST_CASE>

The other available use cases are as follows:

Shell Boot
###########

This use case allows the use of buildroot as the filesystem and boot the software stack on the fast model. FVP will be booted with binaries from the build using the bootloader, firmware image package (fip), kernel and buildroot root file system. It is started by setting <TEST_CASE> to shell.

::

    ./model-scripts/aemfvp-a/boot.sh -p aemfvp-a-rme -b shell

After the boot has been completed, user is expected to see the login prompt.

::

    Welcome to Buildroot

    buildroot login:

A successful boot proves ARM CCA software stack has all components working as
expected on FVP model.

**Note:** Poweroff the guest gracefully with ``poweroff`` command after use.


Validate four World Boot
########################

This use case demonstrates that the stack is able to do a four world boot (Root, Secure, Realm and Non-Secure) described in Arm CCA software. Test boots the software stack and fvp, and parses the log files to validate that four worlds were successfully booted. It is started by setting <TEST_CASE> to validate.

::

    ./model-scripts/aemfvp-a/boot.sh -p aemfvp-a-rme -b validate

**Note:** This use case can be automated.

After the test has been completed, user should see a pass result and log message indicating "Four world boot successful". This test proves all 4 expected Arm CCA worlds i.e Secure, Realm, Normal and root world are running on FVP model.

::

    Secure world boot successful
    Realm world boot successful
    Non-Secure world boot successful
    Four world boot successful


KVM Unit Tests
##############

`KVM-Unit-Tests`_ are set of unit tests for the KVM component. This release includes `Kvm-Unit-Tests-CCA`_ which has a few test for KVM in realm.

Set of kvm-unit-test are developed to test various RME architectural features. Some of these include tests for Realm-RSI (Realm Service Interface), PSCI support in realm security state, GIC to validate interrupt support in the Realm, and Realm attestation to check support for remote attestation for realm payloads. It is started by setting <TEST_CASE> to kvm-unit-tests.

::

    ./model-scripts/aemfvp-a/boot.sh -p aemfvp-a-rme -b kvm-unit-tests

**Note:** This use case can be automated.

This will launch a number of KVM unit tests that are run in Realm security
state.
After the tests have been run, log will indicate the status of individual
kvm test case as follows

::

  RUNNING REALM KVM-UNIT-TESTS
  lkvm run -k ./selftest.flat -m 16 -c 2 --name guest-179
  PASS: selftest: setup: smp: number of CPUs matches expectation
  INFO: selftest: setup: smp: found 2 CPUs
  PASS: selftest: setup: mem: memory size matches expectation
  INFO: selftest: setup: mem: found 16 MB
  SUMMARY: 2 tests

    # KVM session ended normally.
  lkvm run -k ./selftest.flat -m 16 -c 1 --name guest-192
  PASS: selftest: vectors-kernel: und
  PASS: selftest: vectors-kernel: svc
  SKIP: selftest: vectors-kernel: pabt test not supported in a realm
  SUMMARY: 3 tests, 1 skipped

    # KVM session ended normally.
  lkvm run -k ./selftest.flat -m 16 -c 1 --name guest-204
  PASS: selftest: vectors-user: und
  PASS: selftest: vectors-user: svc
  SUMMARY: 2 tests

This completes the Realm kvm-unit-tests run.


Realm Distro Boot Test
######################

This use case boots a Debian VM in the realm security state. This has been tested to boot the Debian cloud image in realm world, where the stock kernel in Debian has been updated with the CCA aware kernel. The Debian cloud image used to boot in the realm state has also been packed into host FS. It is started by setting <TEST_CASE> to realm-distro-boot. 

This use case will boot a Debian machine in realm state and prove that distro boot
is supported in realm security state.

::

    ./model-scripts/aemfvp-a/boot.sh -p aemfvp-a-rme -b realm-distro-boot

On successful boot the user will be presented with Debian login prompt.

::

    Debian GNU/Linux 12 localhost ttyAMA0

    localhost login: root
    Linux localhost 6.7.0-rc4-g19ff5f60db62-dirty #2 SMP PREEMPT Wed Mar  6 16:07:18 UTC 2024 aarch64

    The programs included with the Debian GNU/Linux system are free software;
    the exact distribution terms for each program are described in the
    individual files in /usr/share/doc/*/copyright.

    Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
    permitted by applicable law.
    Last login: Thu Mar  7 14:29:56 UTC 2024 on ttyAMA0

    root@localhost:~#


Realm Guest Test
################

kvmtool is a virtualization utility to launch linux guest images. lkvm also supports creation of realm guests that confirms with ARM RME specs. This virtual machine will be coming instantiated in realm security state.

During the build step of this stack, buildroot packages a kernel and file system so that realm virtual guests can use them. These are found in the path ``/realm`` of the ``host-fs.ext4``. It is started by setting <TEST_CASE> to realm-guest.

::

    ./model-scripts/aemfvp-a/boot.sh -p aemfvp-a-rme -b realm-guest

This will start launching a realm guest and takes some time for it to boot up.
After the boot has been completed, user is expected to see the login prompt.
Realm vm will be logged in with ``root`` as username and password.

::

  Welcome to Buildroot
  buildroot login:

Network Test
###################

This use case ensures networking is enabled between the host machine and lkvm realm vm. After the lkvm guest has been booted up, we check ping and ensure ssh to guest is successful and network to a realm machine is up.

build-scripts copies dropbear ssh keys on both host and guest vm to enable ssh between them. Test sets up a bridge at host and during realm creation an interface is added to this bridge.

Host creates a bridge device vbr0 with IP address 192.168.33.1 and starts a DHCP server so that guests vm can request an IP address in ``192.168.33.XX`` range. Realm VM creation script includes ``--network mode=tap,script=`` where script is used for adding interface to host's bridge. After the realm boot, host establishes ssh connection with IP assigned to realm. It is started by setting <TEST_CASE> to ssh-connection. 

::

    ./model-scripts/aemfvp-a/boot.sh -p aemfvp-a-rme -b ssh-connection

**Note:** This use case can be automated.

This will start a guest and host system and Ping and ssh connection will be succesfull from host to realm vm as follows.

::

    Welcome to Buildroot
    buildroot login: lkvm guest boot complete
    PING 192.168.33.190 (192.168.33.190): 56 data bytes
    --- 192.168.33.190 ping statistics ---
    3 packets transmitted, 3 packets received, 0% packet loss
    ssh:
    Host '192.168.33.190' key accepted unconditionally.
    SSH from Host to Guest Successful
    Stop lkvm guest
    Info: KVM session ended normally.


Run Trusted Firmware-A Tests
############################

`TrustedFrimware-A-Tests`_ (TF-A-Tests) is a suite of baremetal tests to exercise the Trusted Firmware-A (TF-A) features. It enables strong TF-A functional testing without dependency on a Rich OS. It provides a basis for TF-A developers to validate their own platform ports and add their own test cases. In this release, standard tests from TrustedFrimware-A-Tests have been built in. Refer the `Trusted Firmware-A Tests Documentation`_ for more details.

Currently we are running standard TFTF test suite. In accordance with TF documentation tftf has been build with ``ENABLE_REALM_PAYLOAD_TESTS=1`` which builds and packages  tftf for Realm Management Extension (RME) testing. It is started by setting <TEST_CASE> to tftf.

::

    ./model-scripts/aemfvp-a/boot.sh -p aemfvp-a-rme -b tftf

**Note:** This use case can be automated.

This launches entire standard test suite and after tests have been run it's
expected to generate test result as such.

::

  TEST COMPLETE                                                 Passed
  ******************************* Summary *******************************
  > Test suite 'Framework Validation'
                                                                  Passed
  ...
  ...
  > Test suite 'RMI and SPM tests'
                                                                  Passed
  > Test suite 'Realm payload at EL1'
                                                                  Passed
  =================================
  Tests Skipped : 78
  Tests Passed  : 155
  Tests Failed  : 0
  Tests Crashed : 0
  Total tests   : 233
  =================================
  NOTICE:  Exiting tests.

This completes the tftf tests run.


Distro Boot
###########

This use case allows the user to boot a Debian Linux cloud image
on Base AEM FVP. Base aem rme stack supports the boot of Debian.

**Note:** This use case can be automated.

**Note:** This boot is usually more than an hour long on a sophisticated machine.

this test can be executed as follows:

::

    ./model-scripts/aemfvp-a/boot.sh -p aemfvp-a-rme -b distro -d <DISTRO_IMAGE_PATH>

Where <DISTRO_IMAGE_PATH> is the absolute path to the distro image.

When booting from the debian-12-nocloud-arm64.raw image user will be
presented with the login prompt of the distro.

A successful boot on debian-12-nocloud-arm64.raw image with this use case proves UEFI
based boot is supported on Base AEM FVP with Arm CCA Stack.

**Note:** Poweroff the guest gracefully with ``sudo shutdown -h now`` command after use.


Boot the Debian VM in Realm Security State
##########################################

This use case boots a Debian VM in the realm security state. This has been tested to boot the Debian cloud image in realm world, where the stock kernel in Debian has been updated with the CCA aware kernel. The Debian cloud image used to boot in the realm state has also been packed into host FS. It is started by setting <TEST_CASE> to realm-distro-boot.

This use case will boot a Debian machine in realm state and prove that distro boot is supported in realm security state.

::

    ./model-scripts/aemfvp-a/boot.sh -p aemfvp-a-rme -b realm-distro-boot

On successful boot the user will be presented with Debian login prompt.

::

    Debian GNU/Linux 12 localhost ttyAMA0

    localhost login: root
    Linux localhost 6.7.0-rc4-g19ff5f60db62-dirty #2 SMP PREEMPT Wed Mar  6 16:07:18 UTC 2024 aarch64

    The programs included with the Debian GNU/Linux system are free software;
    the exact distribution terms for each program are described in the
    individual files in /usr/share/doc/*/copyright.

    Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
    permitted by applicable law.
    Last login: Thu Mar  7 14:29:56 UTC 2024 on ttyAMA0

    root@localhost:~#

Currently the system is tested with the debian 12 cloud image, The cloud images for Debian can be obtained from the `Debian cloud images`_ page.
A number of images from the previous releases, along with the daily builds of the latest release version are available on this page. The following cloud image has
been used to validate the Debian distribution boot.

- Debian 12 Bookworm cloud image : https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-nocloud-arm64.raw

*Copyright (c) 2024, Arm Limited. All rights reserved.*
*Copyright (c) 2024, Linaro Limited. All rights reserved.*

.. _Booting The System: booting-the-system.rst
.. _TrustedFrimware-A-Tests: https://trustedfirmware-a-tests.readthedocs.io/en/latest/index.html#
.. _Trusted Firmware-A Tests Documentation: https://trustedfirmware-a-tests.readthedocs.io/en/latest/#trusted-firmware-a-tests-documentation
.. _Debian cloud images: https://cloud.debian.org/images/cloud/
